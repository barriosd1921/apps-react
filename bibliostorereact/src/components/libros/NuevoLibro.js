import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';;

const NuevoLibro = ({firestore,history}) => {

    const [titulo,guardarTitulo] = useState('');
    const [isbn,guardarIsbn] = useState('');
    const [editorial,guardarEditorial] = useState('');
    const [existencia,guardarExistencia] = useState(0);

    const guardarLibro = e => {
        e.preventDefault();

        let newLibro = {
            titulo, isbn, editorial, existencia
        };
        
        newLibro.prestados = [];    

        firestore.add({
            collection: 'libros'
        }, newLibro)
        .then(history.push('/'));
    };

    return (
        <div className="row">
            <div className="col-12 mb-4">
                <Link to={'/'} className="btn btn-secondary">
                    <i className="fas fa-arrow circle-left pr-2"></i>
                    Volver al Listado
                </Link>
            </div>
            <div className="col-12">
                <h2>
                    <i className="fas fa-book pr-2"></i>
                    Nuevo Libro
                </h2>

                <div className="row justify-content-center">
                    <div className="col-md-8 mt-5">
                        <form onSubmit={guardarLibro}>
                            <div className="form-group">
                                <label htmlFor="">Titulo:</label>
                                <input type="text" className="form-control" name="titulo" placeholder="Titulo del libro" required onChange={e => guardarTitulo(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">ISBN:</label>
                                <input type="text" className="form-control" name="isbn" placeholder="ISBN del libro" required onChange={e => guardarIsbn(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Existencia:</label>
                                <input type="number" className="form-control" name="existencia" placeholder="Existencia del libro" required onChange={e => guardarExistencia(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Editorial:</label>
                                <input type="text" className="form-control" name="editorial" placeholder="Editorial del libro" required onChange={e => guardarEditorial(e.target.value)}/>
                            </div>

                            <input type="submit" value="Guardar Libro" className="btn btn-block btn-success"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

NuevoLibro.propTypes = {
    firestore: PropTypes.object.isRequired
}

export default firestoreConnect()(NuevoLibro);