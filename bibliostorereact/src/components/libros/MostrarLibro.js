import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import {Link} from 'react-router-dom';
import Spinner from '../layout/Spinner';
import PropTypes from 'prop-types';

const MostrarLibro = ({libro, firestore}) => {

    if(!libro) return <Spinner/>

    // boton para pedir el libro 
    let btnPrestar;
    if(libro.existencia - libro.prestados.length > 0) {
        btnPrestar = <Link to={`/libros/prestar/${libro.id}`} className="btn btn-success my-3">Solicitar Prestamo</Link>;
    } else {
        btnPrestar = null;
    }

    // Boton para devolver libro 
    const devolverLibro = id => {
        const newLibro = {...libro};

        const prestados = libro.prestados.filter(val => val.codigo !== id);

        newLibro.prestados = prestados;

        firestore.update({
            collection: 'libros',
            doc: newLibro.id
        }, newLibro);
    }

    return (
        <div className="row">
            <div className=" col-md-6 mb-4">
                <Link to={'/'} className="btn btn-secondary">
                    <i className="fa fa-arrow-circle-left pr-2"></i>
                    Volver al Listado
                </Link>
            </div>
            <div className="col-md-6 mb-4">
                <Link to={`/libros/editar/${libro.id}`} className="btn btn-primary float-right">
                    <i className="fa fa-pencil-alt pr-2"></i>
                    Editar Libro
                </Link>
            </div>

            <div className="col-12">
                <h2 className="mb-4">
                    {libro.titulo}
                </h2>
                <p><span className="font-weight-bold">ISBN:</span> {libro.isbn}</p>
                <p><span className="font-weight-bold">Editorial:</span> {libro.editorial}</p>
                <p><span className="font-weight-bold">Existencia:</span> {libro.existencia}</p>
                <p><span className="font-weight-bold">Disponibles:</span> {libro.existencia - libro.prestados.length}</p>

                {btnPrestar}
                
                {libro.prestados.length > 0 ? <h3 className="my-2">Personas con libro Prestado</h3> : null}
                {libro.prestados.map(prestado => (
                    <div className="col-md-6">
                        <div className="card my-2" key={prestado.codigo}>
                            <h4 className="card-header">
                                {prestado.nombre} {prestado.apellido}
                            </h4>
                            <div className="card-body">
                                <p>
                                    <span className="font-weight-bold">Codigo:</span> {prestado.codigo}
                                </p>
                                <p>
                                    <span className="font-weight-bold">Carrera:</span> {prestado.carrera}
                                </p>
                                <p>
                                    <span className="font-weight-bold">Fecha de prestamo:</span> {prestado.fecha_solicitud}
                                </p>
                            </div>
                            <div className="card-footer">
                                <button type="button" className="btn btn-success font-weight-bold" onClick={() => devolverLibro(prestado.codigo)}>
                                    Realizar Devolucion
                                </button>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

MostrarLibro.propTypes = {
    firestore: PropTypes.object.isRequired
};

export default compose(
    firestoreConnect(props => [
        {
            collection: 'libros',
            storeAs: 'libro',
            doc: props.match.params.id
        }
    ]),
    connect((state, props) => ({
        libro: state.firestore.ordered.libro && state.firestore.ordered.libro[0]
    }))
)(MostrarLibro);