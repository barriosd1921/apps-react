import React, {useState} from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import {Link} from 'react-router-dom';
import Spinner from '../layout/Spinner';
import PropTypes from 'prop-types';

import FichaSuscriptor from '../suscriptores/FichaSuscriptor';

// Redux actions 
import {buscarUsuario} from '../../actions/BuscarUsuarioAction';

const PrestarLibro = ({libro,firestore,history,usuario,buscarUsuario}) => {

    const [search,guardarSearch] = useState('');
    const [emptyAlumndo,guardarEmptyAlumno] = useState(false);
    
    if(!libro) return <Spinner />
    
    const searchAlumno = e => {
        e.preventDefault();

        const collection = firestore.collection('suscriptores');
        const consulta = collection.where("codigo","==",search).get();

        consulta.then(response => {
            if(response.empty){
                guardarEmptyAlumno(true);
                buscarUsuario({});
            } else {
                const datos = response.docs[0].data();
                buscarUsuario(datos);
                guardarEmptyAlumno(false);
            }
        });
    };

    const solicitarPrestamo = () => {
        usuario.fecha_solicitud = new Date().toLocaleDateString();

        libro.prestados.push(usuario);

        firestore.update({
            collection: 'libros',
            doc: libro.id
        }, libro).then(history.push('/'));
    }

    let fichaAlumno, btnSolicitar;

    if(usuario.nombre){
        fichaAlumno = <FichaSuscriptor alumno={usuario}/>
        btnSolicitar = <button type="button" className="btn btn-primary btn-block" onClick={solicitarPrestamo}>Solicitar Prestamo</button>
    } else {
        fichaAlumno = null;
        btnSolicitar = null;
    }

    // Mostrar mensaje de error 
    let mensajeResultado = '';
    if(emptyAlumndo){
        mensajeResultado = <div className="alert alert-danger mt-3 text-center">No hay Resultado para ese codigo de Alumno</div>
    } else {
        mensajeResultado = null;
    }
    return (
        <div className="row">
            <div className="col-12 mb-4">
                <Link to={'/'} className="btn btn-secondary">
                    <i className="fas fa-arrow circle-left pr-2"></i>
                    Volver al Listado
                </Link>
            </div>
            <div className="col-12">
                <h2>
                    <i className="fas fa-book pr-2"></i>
                    Solicitar Prestamo: {libro.titulo}
                </h2>

                <div className="row justify-content-center mt-5">
                    <div className="col-md-8">
                        <form onSubmit={searchAlumno}>
                            <legend className="color-primary text-center">
                                Busca el Suscriptor por codigo
                            </legend>
                            <div className="form-group">
                                <input type="text" name="search" className="form-control" onChange={e => guardarSearch(e.target.value)} placeholder="Codigo del Alumno"/>
                            </div>
                            <input type="submit" value="Buscar Alumno" className="btn btn-success btn-block"/>
                        </form>

                        {fichaAlumno}
                        {btnSolicitar}
                        {mensajeResultado}
                    </div>
                </div>
            </div>
        </div>
    );
};

PrestarLibro.propTypes = {
    firestore: PropTypes.object.isRequired
};

export default compose(
    firestoreConnect(props => [
        {
            collection: 'libros',
            storeAs: 'libro',
            doc: props.match.params.id
        }
    ]),
    connect((state, props) => ({
        libro: state.firestore.ordered.libro && state.firestore.ordered.libro[0],
        usuario: state.usuario
    }), { buscarUsuario })
)(PrestarLibro);