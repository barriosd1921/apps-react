import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import {Link} from 'react-router-dom';
import Spinner from '../layout/Spinner';
import PropTypes from 'prop-types';

const EditarLibro = ({libro, history, firestore}) => {

    if(!libro) return <Spinner/>

    const titulo = React.createRef();
    const isbn = React.createRef();
    const editorial = React.createRef();
    const existencia = React.createRef();

    const editarLibro = e => {
        e.preventDefault();

        let editLibro = {
            titulo: titulo.current.value, 
            isbn: isbn.current.value, 
            editorial: editorial.current.value, 
            existencia: existencia.current.value
        };

        firestore.update({
            collection: 'libros',
            doc: libro.id
        }, editLibro)
        .then(history.push('/'));
    }
    return (
        <div className="row">
            <div className="col-12 mb-4">
                <Link to={'/'} className="btn btn-secondary">
                    <i className="fas fa-arrow circle-left pr-2"></i>
                    Volver al Listado
                </Link>
            </div>
            <div className="col-12">
                <h2>
                    <i className="fas fa-book pr-2"></i>
                    Editar Libro
                </h2>

                <div className="row justify-content-center">
                    <div className="col-md-8 mt-5">
                        <form onSubmit={editarLibro}>
                            <div className="form-group">
                                <label htmlFor="">Titulo:</label>
                                <input type="text" className="form-control" name="titulo" placeholder="Titulo del libro" required ref={titulo} defaultValue={libro.titulo}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">ISBN:</label>
                                <input type="text" className="form-control" name="isbn" placeholder="ISBN del libro" required ref={isbn} defaultValue={libro.isbn}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Existencia:</label>
                                <input type="number" className="form-control" name="existencia" placeholder="Existencia del libro" required  ref={existencia} defaultValue={libro.existencia}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Editorial:</label>
                                <input type="text" className="form-control" name="editorial" placeholder="Editorial del libro" required  ref={editorial} defaultValue={libro.editorial}/>
                            </div>

                            <input type="submit" value="Editar Libro" className="btn btn-block btn-success"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

EditarLibro.propTypes = {
    firestore: PropTypes.object.isRequired
};

export default compose(
    firestoreConnect(props => [
        {
            collection: 'libros',
            storeAs: 'libro',
            doc: props.match.params.id
        }
    ]),
    connect((state, props) => ({
        libro: state.firestore.ordered.libro && state.firestore.ordered.libro[0]
    }))
)(EditarLibro);