import React, {useState} from 'react';

import {firebaseConnect} from 'react-redux-firebase';
import PropTypes from 'prop-types';

const Login = ({firebase}) => {

    const [email,guardarEmail] = useState('');
    const [password,guardarPassword] = useState('');

    const startSession = e => {
        e.preventDefault();
        firebase.login({
            email,
            password
        }).then( response => {
            console.log('Inicio session');
        }). catch( error => {
            console.log('FALLO INICIO DE SESION');
        });
    }

    return (
        <div className="row justify-content-center">
            <div className="col-md-5">
                <div className="card mt-5">
                    <div className="card-body">
                        <h2 className="text-center py-4">
                            <i className="fas fa-lock pr-2"></i>
                            Iniciar Sesion
                        </h2>
                        <form onSubmit={startSession}>
                            <div className="form-group">
                                <label>Email:</label>
                                <input type="email" className="form-control" name="email" required onChange={e => guardarEmail(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label>Password:</label>
                                <input type="password" className="form-control" name="password" required onChange={e => guardarPassword(e.target.value)}/>
                            </div>

                            <input type="submit" value="Iniciar Sesion" className="btn btn-success btn-block"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

Login.propTypes = {
    firebase: PropTypes.object.isRequired
};

export default firebaseConnect()(Login);