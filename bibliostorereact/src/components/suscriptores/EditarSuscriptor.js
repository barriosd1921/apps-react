import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import {Link} from 'react-router-dom';
import Spinner from '../layout/Spinner';
import PropTypes from 'prop-types';

const EditarSuscriptor = ({suscriptor, firestore, history}) => {

    if(!suscriptor) return <Spinner/>

    const nombre = React.createRef();
    const apellido = React.createRef();
    const carrera = React.createRef();
    const codigo = React.createRef();

    const editarSuscriptor = e => {
        e.preventDefault();

        const editSuscriptor = {
            nombre: nombre.current.value,
            apellido: apellido.current.value,
            carrera: carrera.current.value,
            codigo: codigo.current.value
        };

        firestore.update({
            collection: 'suscriptores', 
            doc: suscriptor.id
        }, editSuscriptor)
        .then(history.push('/suscriptores') );
    }

    return (
        <div className="row">
            <div className="col-12 mb-4">
                <Link to={'/suscriptores'} className="btn btn-secondary">
                    <i className="fas fa-arrow-circle-left pr-2"></i> Volver al Listado
                </Link>
            </div>
            <div className="col-12">
                <h2>
                    <i className="fas fa-user pr-2"></i>
                    Editar Suscriptor
                </h2>
                <div className="row justify-content-center">
                    <div className="col-md-8 mt-5">
                        <form onSubmit={editarSuscriptor}>
                            <div className="form-group">
                                <label>Nombre: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Nombre del suscriptor" required ref={nombre} defaultValue={suscriptor.nombre}/>
                            </div>
                            <div className="form-group">
                                <label>Apellido: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Apellido del suscriptor" required ref={apellido} defaultValue={suscriptor.apellido}/>
                            </div>
                            <div className="form-group">
                                <label>Carrera: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Carrera del suscriptor" required ref={carrera} defaultValue={suscriptor.carrera}/>
                            </div>
                            <div className="form-group">
                                <label>Codigo: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Codigo del suscriptor" required ref={codigo} defaultValue={suscriptor.codigo}/>
                            </div>
                            <input type="submit" value="Editar Suscriptor" className="btn btn-success btn-block"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

EditarSuscriptor.propTypes = {
    firestore: PropTypes.object.isRequired
};

export default compose(
    firestoreConnect(props => [
        {
            collection: 'suscriptores',
            storeAs: 'suscriptor',
            doc: props.match.params.id
        }
    ]),
    connect((state, props) => ({
        suscriptor: state.firestore.ordered.suscriptor && state.firestore.ordered.suscriptor[0]
    }))
)(EditarSuscriptor);