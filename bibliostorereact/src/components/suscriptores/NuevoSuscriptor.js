import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';

const NuevoSuscriptor = ({firestore, history}) => {

    const [nombre,guardarNombre] = useState('');
    const [apellido,guardarApellido] = useState('');
    const [carrera,guardarCarrera] = useState('');
    const [codigo,guardarCodigo] = useState('');

    const guardarSuscriptor = e => {
        e.preventDefault();

        const newSuscriptor = {
            nombre,
            apellido,
            carrera,
            codigo
        };

        firestore.add({ collection: 'suscriptores' },newSuscriptor)
        .then(() =>  history.push('/suscriptores') );
    }

    return (
        <div className="row">
            <div className="col-12 mb-4">
                <Link to={'/suscriptores'} className="btn btn-secondary">
                    <i className="fas fa-arrow-circle-left pr-2"></i> Volver al Listado
                </Link>
            </div>
            <div className="col-12">
                <h2>
                    <i className="fas fa-user-plus pr-2"></i>
                    Nuevo Suscriptor
                </h2>
                <div className="row justify-content-center">
                    <div className="col-md-8 mt-5">
                        <form onSubmit={guardarSuscriptor}>
                            <div className="form-group">
                                <label>Nombre: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Nombre del suscriptor" required 
                                onChange={e => guardarNombre(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label>Apellido: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Apellido del suscriptor" required 
                                onChange={e => guardarApellido(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label>Carrera: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Carrera del suscriptor" required 
                                onChange={e => guardarCarrera(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label>Codigo: </label>
                                <input type="text" name="nombre" id="nombre" className="form-control" placeholder="Codigo del suscriptor" required 
                                onChange={e => guardarCodigo(e.target.value)}/>
                            </div>
                            <input type="submit" value="Agregar Suscriptor" className="btn btn-success btn-block"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

NuevoSuscriptor.propTypes = {
    firestore: PropTypes.object.isRequired
}

export default firestoreConnect()(NuevoSuscriptor);