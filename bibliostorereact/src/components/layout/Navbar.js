import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect} from 'react-redux-firebase';
import {PropTypes} from 'prop-types';

const Navbar = ({auth, firebase}) => {

    const [isAutenticate,guardarIsAutenticate] = useState(false);

    if(auth.uid && !isAutenticate){
        guardarIsAutenticate(true);
    } else if(!auth.uid && isAutenticate) {
        guardarIsAutenticate(false);
    }

    const logOut = () => {
        firebase.logout();
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <nav className="navbar navbar-light">
                <span className="navbar-brand mb-0 h1">Administrador de Biblioteca</span>
            </nav>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            {isAutenticate ? 
                <div className="collapse navbar-collapse" id="navbarColor01">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={'/suscriptores'} className="nav-link">Suscriptores</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/'} className="nav-link">Libros</Link>
                        </li>
                    </ul>
                </div>
            : null}

            {isAutenticate ? 
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a href="#!" className="nav-link">
                            {auth.email}
                        </a>
                    </li>
                    <li className="nav-item">
                        <button type="button" className="btn btn-danger" onClick={logOut}>Cerrar Sesion</button>
                    </li>
                </ul>
            : null}
        </nav>
    );
};

Navbar.propTypes = {
    firebase: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired
};

export default compose(
    firebaseConnect(),
    connect((state,props) => ({
        auth: state.firebase.auth
    }))
)(Navbar);