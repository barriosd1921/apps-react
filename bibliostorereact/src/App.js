import React from 'react';

// Routing 
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Componentes
import Suscriptores from './components/suscriptores/Suscriptores';
import NuevoSuscriptor from './components/suscriptores/NuevoSuscriptor';
import EditarSuscriptor from './components/suscriptores/EditarSuscriptor';
import MostrarSuscriptor from './components/suscriptores/MostrarSuscriptor';
import Navbar from './components/layout/Navbar';
import Libros from './components/libros/Libros';
import MostrarLibro from './components/libros/MostrarLibro';
import EditarLibro from './components/libros/EditarLibro';
import PrestarLibro from './components/libros/PrestarLibro';
import NuevoLibro from './components/libros/NuevoLibro';
import Login from './components/auth/Login';

// Store y provider
import store from './store'; 
import {Provider} from 'react-redux';

// Autenticacion
import {UserIsAuthenticated, UserIsNotAuthenticated} from './helpers/Auth';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navbar/>
          <div className="container mt-4">
            <Switch>
              <Route exact path="/login" component={UserIsNotAuthenticated(Login)}/>

              <Route exact path="/" component={UserIsAuthenticated(Libros)}/>
              <Route exact path="/libros/nuevo" component={UserIsAuthenticated(NuevoLibro)}/>
              <Route exact path="/libros/editar/:id" component={UserIsAuthenticated(EditarLibro)}/>
              <Route exact path="/libros/mostrar/:id" component={UserIsAuthenticated(MostrarLibro)}/>
              <Route exact path="/libros/prestar/:id" component={UserIsAuthenticated(PrestarLibro)}/>

              <Route exact path="/suscriptores" component={UserIsAuthenticated(Suscriptores)}/>
              <Route exact path="/suscriptores/nuevo" component={UserIsAuthenticated(NuevoSuscriptor)}/>
              <Route exact path="/suscriptores/editar/:id" component={UserIsAuthenticated(EditarSuscriptor)}/>
              <Route exact path="/suscriptores/mostrar/:id" component={UserIsAuthenticated(MostrarSuscriptor)}/>            
            </Switch>
          </div>
      </Router>
    </Provider>
  );
}

export default App;
