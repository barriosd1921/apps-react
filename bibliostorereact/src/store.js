import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import { reduxFirestore, firestoreReducer } from 'redux-firestore';
import firebase from 'firebase/app';
import 'firebase/firestore'; 
import 'firebase/auth';

// custom reducers 
import BuscarUsuarioReducer from './reducers/BuscarUsuarioReducer';

// Configurar firestore 
const firebaseConfig = {
    apiKey: "AIzaSyA9TLxsIbTstgkoAS7BU__MGeVFm1s9KhY",
    authDomain: "bibliostore-18683.firebaseapp.com",
    databaseURL: "https://bibliostore-18683.firebaseio.com",
    projectId: "bibliostore-18683",
    storageBucket: "bibliostore-18683.appspot.com",
    messagingSenderId: "962498141323",
    appId: "1:962498141323:web:9e9a243a316d2304ed088e",
    measurementId: "G-RZ2XKLGP1J"
};

// Inicializar firebase 
firebase.initializeApp(firebaseConfig);

// Configurando react-redux 
const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true
};

// enhancer con compose de redux y firestore 
const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase,rrfConfig),
    reduxFirestore(firebase)
)(createStore);

// Reducers 
const rootReducer = combineReducers({
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    usuario: BuscarUsuarioReducer
});

// State inicial 
const initialState = {};

// Creando el Store 
const store = createStoreWithFirebase(rootReducer, initialState, compose(
    reactReduxFirebase(firebase),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

export default store;