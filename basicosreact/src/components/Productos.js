import React, { Component,Fragment } from 'react';
import Producto from "./Producto";

class Productos extends Component {
    state = {
        productos: [
            {id: 1, name: 'Camisa ReactJS', precio: 30},
            {id: 2, name: 'Camisa Laravel', precio: 20},
            {id: 3, name: 'Camisa Angular', precio: 40}
        ]    
    }

    render() {

        const {productos} = this.state;

        return (
            <Fragment>
                <h1>Lista de Productos</h1>
                {productos.map(producto => (
                    <Producto
                        key={producto.id}
                        producto = {producto}
                    />
                ))}
            </Fragment>
        );
    }
}

export default Productos;