import React from 'react';

const Producto = ({producto}) => (
        <li>{`El producto ${producto.name} tiene un valor de ${producto.precio}`}</li>
    );

export default Producto;