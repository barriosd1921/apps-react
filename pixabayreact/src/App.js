import React, {useState, useEffect} from 'react';
import Buscador from './Components/Buscador';
import ListadoImagenes from './Components/ListadoImagenes';
function App() {

  const [busqueda,guardarBusqueda] = useState('');
  const [imagenes,guardarImagenes] = useState([]);

  const [paginaActual, guardarPaginaActual] = useState(1);
  const [totalPaginas,guardarTotalPaginas] = useState(1);

  useEffect(() => {
    const consultarApi = async () => {
      if(busqueda === '') return;
      const imagesPerPage = 30;
      const apiKey = '13530805-60b6387865ba55b8eb9609036';
      const url = `https://pixabay.com/api/?key=${apiKey}&q=${busqueda}&per_page=${imagesPerPage}&page=${paginaActual}`;

      const respuesta = await fetch(url);
      const resultado = await respuesta.json();
      
      guardarImagenes(resultado.hits);

      //calcular el total de paginas
      guardarTotalPaginas(Math.ceil(resultado.totalHits/imagesPerPage));
    
      // hacer scroll top 
      const jumbotron = document.querySelector('.jumbotron');
      jumbotron.scrollIntoView({behavior:'smooth', block:'end'});
    }

    consultarApi();

  },[busqueda, paginaActual]);

  const paginaAnterior = () => {
    let nuevaPaginaActual = paginaActual - 1;
    // colocarlo en el state 
    guardarPaginaActual(nuevaPaginaActual);
  }

  const paginaSiguiente = () => {
    let nuevaPaginaActual = paginaActual + 1;
    guardarPaginaActual(nuevaPaginaActual);
  }

  return (
    <div className="app container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de imagenes</p>
        <Buscador 
          guardarBusqueda = {guardarBusqueda}
        />
      </div>
      <div className="row justify-content-center">
        <ListadoImagenes 
          imagenes={imagenes}
        />
        { (paginaActual === 1) ? null : <button onClick={paginaAnterior} type="button" className="btn btn-info mr-1">&laquo; Anterior</button>}
        { (paginaActual === totalPaginas) ? null: <button onClick={paginaSiguiente} type="button" className="btn btn-info">Siguiente &raquo;</button>}
        
      </div>
    </div>
  );
}

export default App;
