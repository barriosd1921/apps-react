import React, {useEffect} from 'react';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {getProductosAction} from '../actions/productosAction';

// Componentes
import Producto from './Producto';

const Productos = () => {

    // Llamar accion de traer productos 
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProductosAction());
    }, []);

    //  Acceder al state 
    const loading = useSelector(state => state.productos.loading);
    const error = useSelector(state => state.productos.error);
    const productos = useSelector(state => state.productos.productos);

    return (
        <React.Fragment>
            
            { error ? <div className="font-weight-bold alert alert-danger text-center mt-4">Hubo un ERROR</div> : null }
                <h2 className="text-center my-5">Listado de Productos</h2>

                <table className="table table-striped">
                    <thead className="bg-primary table-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Acciones</th>
                        </tr>   
                    </thead>
                    <tbody>
                        {productos.map(producto => (
                            <Producto 
                                key={producto.id}
                                producto={producto}
                            />
                        ))}
                    </tbody>
                </table>

                { loading ? 'Cargando...' : null}
        </React.Fragment>
    );
};

export default Productos;