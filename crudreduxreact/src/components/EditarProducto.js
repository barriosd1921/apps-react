import React, {useEffect, useRef} from 'react';

import {useDispatch, useSelector} from 'react-redux';
import {editProductoAction, executeEdtitProductoAction} from '../actions/productosAction';
import {validarFormularioAction} from '../actions/validacionAction';


import Swal from 'sweetalert2';

const EditarProducto = ({match, history}) => {

    // Crear los ref 
    const nombreRef = useRef('');
    const precioRef = useRef('');

    const dispatch =  useDispatch();
    const {id} = match.params;

    useEffect(() => {
        dispatch( editProductoAction(id) );
    },[dispatch, id]);

    // Acceder al state 
    const producto = useSelector(state => state.productos.producto);
    const error = useSelector(state => state.productos.error);

    const submitEditarProducto = e => {
        e.preventDefault();

        const toEdit = {
            id,
            nombre: nombreRef.current.value,
            precio: precioRef.current.value
        }

        // Validar formulario
        dispatch( validarFormularioAction() );
        if(toEdit.nombre.trim() === '' || toEdit.precio.trim() === ''){
            validarFormularioAction(true);
            return;
        }
        else{
            validarFormularioAction(false);
        }

        // Guardar Cambios
        dispatch( executeEdtitProductoAction(toEdit) );

        Swal.fire(
            'Almacenado',
            'El producto se actualizo Correctamente.',
            'success'
        );
        
        // Redireccionar
        history.push('/');
    }
    // Cuando carga la api
    if(!producto) return 'Cardando...';

    return (
        <div className="row justify-content-center mt-5">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center">Editar Producto</h2>
                        <form onSubmit={submitEditarProducto}>
                            <div className="form-group">
                                <label>Titulo</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    placeholder="Titulo"
                                    defaultValue={producto.nombre}
                                    ref={nombreRef}
                                />
                            </div>
                            <div className="form-group">
                                <label>Precio del Producto</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    placeholder="Precio"
                                    defaultValue={producto.precio}
                                    ref={precioRef}
                                />
                            </div>

                            <button type="submit" className="btn btn-primary font-weight-bold text-uppercase d-block w-100">Guardar Cambios</button>
                        </form>

                        { error ? <div className="font-weight-bold alert alert-danger text-center mt-4">Hubo un error, intenta de nuevo</div> : null}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditarProducto;