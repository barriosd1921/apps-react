import React from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

// Redux
import {useDispatch} from 'react-redux';
import {deleteProductoAction} from '../actions/productosAction';

const Producto = ({producto}) => {

    // dispatch para ejecutar acciones
    const dispatch = useDispatch ();

    const confirmDelete = id => {

        // Confirmacion de borrado
        Swal.fire({
            title: '¿Estas seguro?',
            text: "Un producto eliminado no se puede eliminar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonText: "Cancelar"
        }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Eliminado',
                'El producto se Elimino correctamente.',
                'success'
            )
            dispatch( deleteProductoAction(id) );
        }
        })
    }
    return (
        <tr>
            <td>
                {producto.nombre}
            </td>
            <td>
                <span className="font-weight-bold">$ {producto.precio}</span>
            </td>
            <td className="acciones">
                <Link to={`/productos/editar/${producto.id}`} className="btn btn-primary mr-2">
                    Editar
                </Link>
                <button className="btn btn-danger" onClick={() => confirmDelete(producto.id)}>
                    Eliminar
                </button>
            </td>
        </tr>
    );
};

export default Producto;