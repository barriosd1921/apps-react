import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_ERROR,
    AGREGAR_PRODUCTO_EXITO,
    DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR,
    PREPARE_TO_DELETE,
    DELETE_PRODUCTO_EXITO,
    DELETE_PRODUCTO_ERROR,
    GET_PRODUCTO_EDIT,
    PRODUCTO_EDIT_EXITO,
    PRODUCTO_EDIT_ERROR,
    PREPARE_EDIT_PRODUCTO,
    EDIT_PRODUCTO_EXITO,
    EDIT_PRODUCTO_ERROR
} from '../types';

import clientAxios from '../config/axios';

// Crear un nuevo producto - Funcion Principal

export function crearNuevoProductoAction(producto) {
    return (dispatch) => {
        dispatch( nuevoProducto() );

        // Insertar datos en la api 
        clientAxios.post('/libros', producto)
            .then(response => {
                console.log(response);
                dispatch( agregarProductoExito(producto) );
            })
            .catch(error => {
                console.log(error);
                dispatch( agregarProductoError(error) );
            })
    }
}

export const nuevoProducto = () => ({
    type: AGREGAR_PRODUCTO
})

export const agregarProductoExito = producto => ({
    type: AGREGAR_PRODUCTO_EXITO,
    payload: producto
})

export const agregarProductoError = error => ({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: error
})

// Obtener listado de productos (consultar api)
export function getProductosAction() {
    return (dispatch) => {
        dispatch(initGetProductos());

        // Consultando la api 
        clientAxios.get('/libros')
            .then(response => {                
                // console.log(response);
                dispatch( getProductosSuccess(response.data) );
            })
            .catch(error => {
                // console.log(error);
                dispatch( getProductosError() );
            });
    }
}

export const initGetProductos = () => ({
    type: DESCARGA_PRODUCTOS
})

export const getProductosSuccess = (productos) => ({
    type: DESCARGA_PRODUCTOS_EXITO,
    payload: productos
})

export const getProductosError = () => ({
    type: DESCARGA_PRODUCTOS_ERROR
})

// Funccion para elimiar producto en especifico

export function deleteProductoAction(id) {
    return (dispatch) => {
        dispatch( prepareProductoDelete() );

        // Eliminar en la Api 
        clientAxios.delete(`/libros/${id}`)
            .then(response => {
                dispatch( deleteProductoSuccess(id) );
            })
            .catch(error => {
                dispatch( deleteProductoError() );
            });
    }
}

export const prepareProductoDelete = () => ({
    type: PREPARE_TO_DELETE
})

export const deleteProductoSuccess = (id) => ({
    type: DELETE_PRODUCTO_EXITO,
    payload: id
})

export const deleteProductoError = () => ({
    type: DELETE_PRODUCTO_ERROR
})

// Function principal para obtener producto a editar 

export function editProductoAction(id) {
    return (dispatch) => {
        dispatch( getProductoEdit() );

        // Obtener producto de la api 
        clientAxios.get(`/libros/${id}`)
            .then(response => {
                console.log(response.data);
                dispatch( getProductoEditSuccess(response.data) );
            })
            .catch(error => {
                console.log(error);
                dispatch( getProductoEditError() );
            });
    }
}

export const getProductoEdit = () => ({
    type: GET_PRODUCTO_EDIT
})

export const getProductoEditSuccess = (producto) => ({
    type: PRODUCTO_EDIT_EXITO,
    payload: producto
})

export const getProductoEditError = () => ({
    type: PRODUCTO_EDIT_ERROR
})

// Function principal para editar producto 
export function executeEdtitProductoAction(producto) {
    return (dispatch) => {
        dispatch( startEditProducto() );

        // Consultando la api 
        clientAxios.put(`/libros/${producto.id}`, producto)
            .then(response => {
                dispatch( editProductoSuccess(response.data) );
            })
            .catch(error => {
                dispatch( editProductoError() );
            });
    }
}

export const startEditProducto = () => ({
    type: PREPARE_EDIT_PRODUCTO
})

export const editProductoSuccess = producto => ({
    type: EDIT_PRODUCTO_EXITO,
    payload: producto
})

export const editProductoError = () => ({
    type: EDIT_PRODUCTO_ERROR
})