import {
    VALIDAR_FORMULARIO,
    VALIDAR_FORMULARIO_EXITO,
    VALIDAR_FORMULARIO_ERROR
} from '../types';

export function validarFormularioAction(error) {
    return (dispatch) => {
        dispatch( iniciarValidacion() );

        if(error)
            dispatch( validacionError() );
        else
            dispatch( validacionExito() );
    }
}

export const iniciarValidacion = () => {
    return {
        type: VALIDAR_FORMULARIO
    }
}

export const validacionExito = () => {
    return {
        type: VALIDAR_FORMULARIO_EXITO
    }
}

export const validacionError = () => {
    return {
        type: VALIDAR_FORMULARIO_ERROR
    }
}