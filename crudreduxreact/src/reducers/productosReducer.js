import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_ERROR,
    AGREGAR_PRODUCTO_EXITO,
    DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR,
    PREPARE_TO_DELETE,
    DELETE_PRODUCTO_EXITO,
    DELETE_PRODUCTO_ERROR,
    GET_PRODUCTO_EDIT,
    PRODUCTO_EDIT_EXITO,
    PRODUCTO_EDIT_ERROR,
    PREPARE_EDIT_PRODUCTO,
    EDIT_PRODUCTO_EXITO,
    EDIT_PRODUCTO_ERROR
} from '../types';

// Cada reducer tiene su propio state 

const initialState = {
    productos: [],
    error: null,
    loading: false,
    producto: {}
};

export default function(state = initialState, action) {
    switch(action.type){
        case AGREGAR_PRODUCTO:
            return {
                ...state,
                error: null
            }
        case AGREGAR_PRODUCTO_EXITO:
            return {
                ...state,
                error: null,
                productos: [...state.productos, action.payload]
            }
        case AGREGAR_PRODUCTO_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case DESCARGA_PRODUCTOS:
            return {
                ...state,
                loading: true,
                producto: {}
            }
        case DESCARGA_PRODUCTOS_EXITO:
            return {
                ...state,
                productos: action.payload,
                loading: false,
                error: false,
                producto: {}
            }
        case DESCARGA_PRODUCTOS_ERROR:
            return {
                ...state,
                productos: [],
                error: true,
                loading: false,
                producto: {}
            }
        case PREPARE_TO_DELETE:
            return {
                ...state,
                error: null
            }
        case DELETE_PRODUCTO_EXITO:
            return {
                ...state,
                error: null,
                productos: state.productos.filter(val => val.id !== action.payload)
            }
        case DELETE_PRODUCTO_ERROR:
            return {
                ...state,
                error: true
            }
        case GET_PRODUCTO_EDIT:
            return {
                ...state,
                error: null
            }
        case PRODUCTO_EDIT_EXITO:
            return {
                ...state,
                error: null,
                producto: action.payload
            }
        case PRODUCTO_EDIT_ERROR:
            return {
                ...state,
                error: true
            }
        case PREPARE_EDIT_PRODUCTO:
            return {
                ...state,
                error: null
            }
        case EDIT_PRODUCTO_EXITO:
            return {
                ...state,
                error: null,
                productos: state.productos.map(producto => producto.id === action.payload.id ? producto = action.payload : producto)
            }
        case EDIT_PRODUCTO_ERROR:
            return {
                ...state,
                error: true
            }
        default:
            return state;
    }
} 