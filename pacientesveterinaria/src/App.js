import React, {Component} from 'react';
import Header from './Components/Header';
import NuevaCita from './Components/NuevaCita';
import ListaCitas from './Components/ListaCitas';
import './bootstrap.min.css';

class App extends Component {
  state = {
    citas: []
  }
  // Cuando la aplicacion carga
  componentDidMount() {
    const citasLS = localStorage.getItem('citas');
    if(citasLS) {
      this.setState({
        citas: JSON.parse(citasLS)
      });
    }
  }
  
  // Cuando agregamos o eliminamos una nueva cita 
  componentDidUpdate(prevProps, prevState) {
    localStorage.setItem('citas', JSON.stringify(this.state.citas));
  }
  

  crearNuevaCita = datos => {
    // copiar el state actual
    const citas = [...this.state.citas, datos];

    // agregar el nuevo state 
    this.setState({
      citas
    });
  }

  // Eliminar citas del state 
  eliminarCita = id => {
    // Hacer copia del state 
    const citasActuales = [...this.state.citas];

    // utilizar filter para sacar el elemento del arreglo
    const citas = citasActuales.filter(cita => cita.id !== id);

    //Actualicar el state
    this.setState({
      citas
    });
  }

  render() {
    return (
      <div className="container">
        <Header 
          titulo = 'Administrador Pacientes Veterinaria'
        />
        <div className="row">
          <div className="col-md-10 mx-auto">
            <NuevaCita
              crearNuevaCita={this.crearNuevaCita}
            />
          </div>

          <div className="mt-5 col-md-10 mx-auto">
            <ListaCitas
              citas = {this.state.citas}
              eliminarCita = {this.eliminarCita}
            />
          </div>    
        </div>
        
      </div>
    );
  }
}

export default App;
