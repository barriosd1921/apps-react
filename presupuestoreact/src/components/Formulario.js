import React, { useState }  from 'react';
import Error from './Error';
import shortid from 'shortid';

const Formulario = (props) => {

    const {guardarGasto, guardarCrearGasto} = props;
    
    const [nombreGasto, guardarNombreGasto] = useState('');
    const [cantidadGasto, guardarCantidadGasto ]=useState(0);
    const [error,guardarError] = useState(false);

    
    const agregarGasto = e => {
        e.preventDefault();

        // Validar
        if(cantidadGasto <= 0 || !cantidadGasto || isNaN(cantidadGasto) || nombreGasto === ''){
            guardarError(true);
            return;
        }

        // construir objeto de gasto 
        const gasto = {
            nombreGasto,
            cantidadGasto,
            id: shortid.generate()
        }

        // Pasar al componente Principal
        guardarGasto(gasto);
        guardarCrearGasto(true);
        guardarError(false);
        guardarNombreGasto('');
        guardarCantidadGasto('');
    }
    return (
        <form
            onSubmit={agregarGasto}
        >
            <h2>Agrega tus gatos aqui</h2>

            {(error) ? <Error mensaje="Campos incorrectos y/o Obligatorios"/>: null}

            <div className="campo">
                <label htmlFor="">Nombre Gasto</label>
                <input 
                    type="text"
                    className="u-full-width"
                    placeholder="Ej. Transporte"
                    onChange={e => guardarNombreGasto(e.target.value)}
                    value={nombreGasto}
                />

                <label htmlFor="">Cantidad Gasto</label>
                <input 
                    type="number"
                    className="u-full-width"
                    placeholder="Ej. 300"
                    onChange={e => guardarCantidadGasto(parseInt(e.target.value,10))}
                    value={cantidadGasto}
                />

                <input 
                    type="submit"
                    value="Agregar Gasto"
                    className="button-primary u-full-width"
                />
            </div>
        </form>
    );
};

export default Formulario;