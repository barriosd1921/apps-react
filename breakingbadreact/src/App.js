import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Frase from './components/Frase';

function App() {

  const [frase, obtenerFrase] = useState({});

  const consultarAPI = async () => {
    const resultado = await axios('https://breaking-bad-quotes.herokuapp.com/v1/quotes');

    // Agregar resultado de la api al state 
    obtenerFrase(resultado.data[0]);
  }

  useEffect( () => {
      consultarAPI();
  }, []);

  return (
    <div className="contenedor">
      <Frase 
        frase = {frase}
      />

      <button
        onClick={consultarAPI}
      >Generar Nueva</button>
    </div>
  );
}

export default App;
