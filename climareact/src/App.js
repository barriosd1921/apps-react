import React, { Fragment, useState, useEffect } from 'react';

import Header from './components/Header';
import Formulario from './components/Formulario';

function App() {

  const [busqueda,setBusqueda ] = useState ({
      ciudad: '',
      pais: ''
  });

  const [consultar,setConsultar] = useState(false);

  const {ciudad,pais} = busqueda;

  useEffect( () => {
    const consultarAPI = async () => {
      const appId = 'fb8095a033543ed72f16f75960433bed';
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;
      
      if(consultar) {
        const response = await fetch(url);
        const resultado = await response.json();
        console.log(resultado);
      }
    }
    consultarAPI();
  },[consultar]);

  return (
    <Fragment>
      <Header
        title = "Clima React App"
      />

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario 
                busqueda={busqueda}
                setBusqueda={setBusqueda}
                setConsultar={setConsultar}
              />
            </div>
            <div className="col m6 s12">
              2
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
