import React from 'react';

import styled from "@emotion/styled";

const ContainerResumen = styled.div`
    padding: 1rem;
    text-align: center;
    background-color: #00838F;
    color: #FFF;
    margin-top: 1rem;
`;

const Resumen = ({datos}) => {

    if(!datos) return null;

    return (
        <ContainerResumen>
            <h2>Resumen de Cotización</h2>
            <ul>
                <li>Marca: {datos.marca}</li>
                <li>Plan: {datos.plan}</li>
                <li>Año: {datos.year}</li>
            </ul>
        </ContainerResumen>
    );
};

export default Resumen;