import React from 'react';

import styled from "@emotion/styled";

const Mensaje = styled.p`
    background-color: rgb(127,224,237);
    margin-top: 2rem;
    padding: 1rem;
    text-align: center;
`;

const ResultadoCoti = styled.div`
     background-color: rgb(127,224,237);
     padding: .5rem;
     border: 1px solid #26C6DA;
     text-align: center;
     margin-top: 1rem;
     position: relative;
`;

const Total = styled.p`
    color: #00838F;
    padding: 1rem;
    text-transform: uppercase;
    font-weight: bold;
    margin: 0px;
`;

const Resultado = ({resumen}) => {


    return (
    (!resumen) ? <Mensaje>Elige Marca, Año y Plan para cotizar</Mensaje> : 
        (
            <ResultadoCoti>
                <Total>El total es: ${resumen}</Total>
            </ResultadoCoti>
        )
    );
};

export default Resultado;