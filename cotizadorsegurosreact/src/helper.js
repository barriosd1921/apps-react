export function getDiffYear( year ) {
    return new Date().getFullYear() - year;
}

export function getPriceByMarca (marca) {
    let price;

    switch(marca) {
        case 'europeo': 
            price = 1.3;
        break;
        case 'americano':
            price = 1.15;
        break;
        case 'asiatico':
            price = 1.05;
        break;
        default:
            price = 1;
        break;
    }

    return price;
} 

export function getPriceBySeguro(plan) {
    return (plan === 'Basico') ? 1.2 : 1.5;
}