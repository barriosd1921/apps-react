import React, { useState } from 'react';

import Header from './components/Header';
import Formulario from './components/Formulario';
import Resumen from './components/Resumen';
import Resultado from './components/Resultado';

import styled from "@emotion/styled";

const Contenedor = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const ContenedorForm = styled.div`
  background-color: #FFF;
  padding: 3rem;
`;

function App() {

  const [resumen, setResumen ] = useState({});

  return (
    <Contenedor>
      <Header 
        titulo="Cotizador de Seguros"
      />

      <ContenedorForm>
        <Formulario 
            setResumen = {setResumen}
        />

        <Resumen
          datos = {resumen.datos}
        />

        <Resultado
          resumen = {resumen.cotizacion}
        />
      </ContenedorForm>
    </Contenedor>
  );
}

export default App;
