import React, {useEffect, useState} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from 'axios';

import Productos from './Components/Productos';
import EditarProducto from './Components/EditarProducto';
import AgregarProducto from './Components/AgregarProducto';
import Producto from './Components/Producto';
import Header from './Components/Header';

function App() {

  const [productos, guardarProductos] = useState([]);
  const [reload,guardarReload] = useState(true);

  useEffect(() => {
    if(reload) {
      const consultarApi = async () => {
        // consultar api local de json-server
        const response = await axios.get('http://localhost:4000/restaurant');
        guardarProductos(response.data);
      }
  
      consultarApi();

      guardarReload(false);
    }
    

  },[reload]);

  return (
    <Router>
      <Header />
      <main className="container mt-5">
        <Switch>
          <Route exact path="/nuevoProducto" 
            render={() => (
              <AgregarProducto 
                guardarReload = {guardarReload}
              />
            )}
          />
          <Route exact path="/productos" 
            render={() => (
              <Productos 
                productos={productos}
                guardarReload = {guardarReload}
              />
            )}
          />
          <Route exact path="/productos/:id" component={Producto}/>
          <Route exact path="/productos/editar/:id"
            render={props => {
              // Tomar el id del producto 
              const idProducto = parseInt(props.match.params.id);
              
              // el producto que se pasa al state 
              const producto = productos.filter(producto => producto.id === idProducto);
              
              return (
                <EditarProducto 
                  producto={producto[0]}
                  guardarReload = {guardarReload}
                />
              )
            }}
          />
        </Switch>
      </main>
      <p className="mt-4 p-2 text-center">Todos los derechos reservados</p>
    </Router>
  );
}

export default App;
