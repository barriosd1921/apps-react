import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import {withRouter} from 'react-router-dom';

const ProductoLista = ({history, producto, guardarReload}) => {

    const eliminarProducto = id => {
        console.log(id);

        Swal.fire({
            title: '¿Estas seguro?',
            text: "Un platillo eliminado no se puede recuperar.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonText: 'Cancelar'
        }).then( async (result) => {
            try {
                const url = `http://localhost:4000/restaurant/${id}`;

                const response = await axios.delete(url);

                if(response.status === 200) {
                    if (result.value) {
                        Swal.fire(
                        'Eliminado',
                        'El platillo se ha eliminado.',
                        'success'
                        )
                    }
                }
            } catch (error) {
                console.log(error);
                Swal.fire(
                    'Error',
                    'Vuelve a intentarlo',
                    'error' 
                );
            }
            
            //redirijo al usuario
            guardarReload(true);
            history.push('/productos');
        })
    }

    return (
        <li data-categoria={producto.categoria} className="list-group-item d-flex justify-content-between align-items-center">
            <p>
                {producto.nombre}
                <span className="font-weight-bold pl-2">${producto.precio}</span>
            </p>
            <div>
                <Link
                    to={`/productos/editar/${producto.id}`}
                    className="btn btn-success mr-2"
                >Editar</Link>

                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => eliminarProducto(producto.id)}
                >
                    Eliminar &times;
                </button>
            </div>
        </li>
    );
};

export default withRouter(ProductoLista);