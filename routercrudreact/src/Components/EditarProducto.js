import React, {useState, useRef} from 'react';
import Swal from 'sweetalert2';
import axios from 'axios';
import {withRouter} from 'react-router-dom';

import Error from './Error';

const EditarProducto = ({history, producto, guardarReload}) => {

    // generar el ref 
    const precio = useRef('');
    const nombre = useRef('');

    // States 
    const [categoria,guardarCategoria] = useState('');
    const [error, guardarError] = useState(false);

    const leerRadio = e => {
        guardarCategoria(e.target.value);
    }

    const editarProducto = async e => {
        e.preventDefault();

        let nuevaCategoria = (categoria === '') ? producto.categoria : categoria;
        
        const editarPlato = {
            precio: precio.current.value,
            nombre: nombre.current.value,
            categoria: nuevaCategoria
        }

        if(editarPlato.nombre === '' || editarPlato.precio === '' || editarPlato.categoria === '') {
            guardarError(true);
            return;
        }

        guardarError(false);
        
        // Enviar el request 
        const url = `http://localhost:4000/restaurant/${producto.id}`;

        try {
            const response = await axios.put(url, editarPlato);

            if(response.status === 200) {
                Swal.fire(
                       'Producto Editado',
                       'El producto se edito correctamente',
                       'success' 
                );
            }

        } catch (error) {
            console.log(error);
            Swal.fire(
                'Error',
                'Vuelve a intentarlo',
                'error' 
            );
        }

        //redirijo al usuario
        guardarReload(true);
        history.push('/productos');
    }

    return (
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Editar Producto</h1>

            {(error) ? <Error mensaje='Todos los campos son obligatorios'/> : null}
            
            <form
                className="mt-5"
                onSubmit={editarProducto}
            >
                <div className="form-group">
                    <label>Nombre Platillo</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="nombre" 
                        placeholder="Nombre Platillo"
                        ref={nombre}
                        defaultValue={producto.nombre}
                    />
                </div>

                <div className="form-group">
                    <label>Precio Platillo</label>
                    <input 
                        type="number" 
                        className="form-control" 
                        name="precio"
                        placeholder="Precio Platillo"
                        ref={precio}
                        defaultValue={producto.precio}
                    />
                </div>

                <legend className="text-center">Categoría:</legend>
                <div className="text-center">
                    <div className="form-check form-check-inline">
                        <input 
                            className="form-check-input" 
                            type="radio" 
                            name="categoria"
                            value="postre"
                            onChange={leerRadio}
                            defaultChecked={(producto.categoria) === 'postre'}
                        />
                        <label className="form-check-label">
                            Postre
                        </label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input 
                            className="form-check-input" 
                            type="radio" 
                            name="categoria"
                            value="bebida"
                            onChange={leerRadio}
                            defaultChecked={(producto.categoria) === 'bebida'}
                        />
                        <label className="form-check-label">
                            Bebida
                        </label>
                    </div>

                    <div className="form-check form-check-inline">
                        <input 
                            className="form-check-input" 
                            type="radio" 
                            name="categoria"
                            value="cortes"
                            onChange={leerRadio}
                            defaultChecked={(producto.categoria) === 'cortes'}
                        />
                        <label className="form-check-label">
                            Cortes
                        </label>
                    </div>

                    <div className="form-check form-check-inline">
                        <input 
                            className="form-check-input" 
                            type="radio" 
                            name="categoria"
                            value="ensalada"
                            onChange={leerRadio}
                            defaultChecked={(producto.categoria) === 'ensalada'}
                        />
                        <label className="form-check-label">
                            Ensalada
                        </label>
                    </div>
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Editar Producto" />
            </form>
        </div>
    );
};

export default withRouter(EditarProducto);