import React, { useState, useEffect } from 'react';
import Criptomoneda from './Criptomoneda';
import Error from './Error';

import axios from 'axios';

const Formulario = ({guardarMoneda,guardarCriptoMoneda}) => {

    const [cripto, guardarCripto] = useState([]);
    const [monedaCotizar, guardarMonedaCotizar] =  useState('');
    const [criptoCotizar, guardarCriptoCotizar] = useState('');
    const [error, guardarError] = useState(false);

    useEffect (
        () => {
            const consultarApi =  async () => {
                const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=15&tsym=USD`;

                const response = await axios.get(url);

                guardarCripto(response.data.Data);
            }

            consultarApi();
        } , []
    );
    
    const cotizarMoneda = e => {
        e.preventDefault();
        if(monedaCotizar === '' || criptoCotizar === '') {
            guardarError(true);
            return;
        } else {
            guardarError(false);
            guardarMoneda(monedaCotizar);
            guardarCriptoMoneda(criptoCotizar);
        }

    };

    // Mostrar el error en case de que exista
    const componente = (error) ? <Error  mensaje = 'Ambos campos son obligatorios'/> : null;
    
    return (
        <form
            onSubmit={cotizarMoneda}
        >   
            {componente}
            <div className="row">
                <label htmlFor="monedaCotizar">Elige tu Modena</label>

                <select 
                    name="monedaCotizar" 
                    id="monedaCotizar" 
                    className="u-full-width"
                    onChange={ e => {guardarMonedaCotizar(e.target.value)}}
                >
                    <option value="">-- Elige Tu Moneda --</option>
                    <option value="USD">Dolar Estado Unidense</option>
                    <option value="MXN">Peso Mexicano</option>
                    <option value="GBP">Libras</option>
                    <option value="EUR">Euro</option>
                    <option value="COP">Peso Colombiano</option>
                </select>
            </div>

            <div className="row">
                <label htmlFor="criptoCotizar">Elige tu Criptomoneda</label>

                <select 
                    name="criptoCotizar" 
                    id="criptoCotizar" 
                    className="u-full-width"
                    onChange={ e => { guardarCriptoCotizar(e.target.value)}}
                >
                    <option value="">-- Elige Tu CriptoMoneda --</option>
                    {cripto.map(cri => (
                        <Criptomoneda 
                            key={cri.CoinInfo.Id}
                            criptomoneda = {cri}
                        />
                    ))}
                </select>
            </div>

            <input type="submit" className="button-primary u-full-width" value="Cotizar"/>
        </form>
    );
};

export default Formulario;