import React, {useState, useEffect} from 'react';
import axios from 'axios';

import Formulario from './components/Formulario';
import Spinner from './components/Spinner';
import Resultado from './components/Resultado';

import imagen from './cryptomonedas.png';

function App() {

  const [moneda, guardarMoneda] = useState('');
  const [criptoMoneda, guardarCriptoMoneda] = useState('');
  const [cargando, guardarCargando] = useState(false);
  const [resultado,guardarResultado] = useState({});

  useEffect(
    () => {
      const cotizarCriptoMoneda = async () => {

        if(moneda === '') return;

        const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptoMoneda}&tsyms=${moneda}`;
        
        const response = await axios.get(url);

        guardarCargando(true);

        setTimeout(() => {
          guardarCargando(false);
          guardarResultado(response.data.DISPLAY[criptoMoneda][moneda]);
        },3000);
      }

      cotizarCriptoMoneda();
    }, [moneda,criptoMoneda]
  );

  // Mostrar Spinner o resultado
  const componente = (cargando) ? <Spinner /> : <Resultado resultado={resultado}/>;
  
  return (
    <div className="container">
      <div className="row">
        <div className="one-half column">
          <img src={imagen} alt="Imagen criptomonedas" className="logotipo"/>
        </div>
        <div className="one-half column">
          <h1>Cotiza Criptomonedas al instante</h1>

          <Formulario 
            guardarMoneda={guardarMoneda}
            guardarCriptoMoneda={guardarCriptoMoneda}
          />

          {componente}

        </div>
      </div>
    </div>
  );
}

export default App;
