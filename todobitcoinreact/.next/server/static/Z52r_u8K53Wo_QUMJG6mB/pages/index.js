module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("RNiq");


/***/ }),

/***/ "0bYB":
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "RNiq":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./components/Master.js + 1 modules
var Master = __webpack_require__("pJ0N");

// CONCATENATED MODULE: ./components/Precio.js

var __jsx = external_react_default.a.createElement;

const Precio = props => {
  const {
    price,
    percent_change_1h,
    percent_change_24h,
    percent_change_7d
  } = props.precio;
  return __jsx("div", {
    className: "card text-white bg-success mb-3"
  }, __jsx("div", {
    className: "card-header"
  }, "Precio del bitCoin"), __jsx("div", {
    className: "card-body"
  }, __jsx("h4", {
    className: "card-title"
  }, "Precio Actual: $ ", price.toFixed(2)), __jsx("div", {
    className: "d-md-flex justify-content-between"
  }, __jsx("p", {
    className: "card-text"
  }, __jsx("span", {
    className: "font-weight-bold"
  }, "Ultima Hora: "), percent_change_1h, " %"), __jsx("p", {
    className: "card-text"
  }, __jsx("span", {
    className: "font-weight-bold"
  }, "Ultimas 24 Horas: "), percent_change_24h, " %"), __jsx("p", {
    className: "card-text"
  }, __jsx("span", {
    className: "font-weight-bold"
  }, "Ultimos 7 Dias: "), percent_change_7d, " %"))));
};

/* harmony default export */ var components_Precio = (Precio);
// CONCATENATED MODULE: ./components/Noticia.js

var Noticia_jsx = external_react_default.a.createElement;

const Noticia = props => {
  const {
    urlToImage,
    url,
    title,
    description,
    source
  } = props.noticia;
  let imagen;
  if (urlToImage !== '') imagen = Noticia_jsx("img", {
    src: urlToImage,
    alt: title,
    className: "card-img-top"
  });else imagen = Noticia_jsx("p", {
    className: "text-center my-5"
  }, "Imagen No Disponible");
  return Noticia_jsx("div", {
    className: "col-md-6 col-12 mb-4"
  }, Noticia_jsx("div", {
    className: "card"
  }, Noticia_jsx("div", {
    className: "card-image"
  }, imagen), Noticia_jsx("div", {
    className: "card-body"
  }, Noticia_jsx("h3", {
    className: "card-title"
  }, title), Noticia_jsx("p", {
    className: "card-text"
  }, description), Noticia_jsx("p", {
    className: "card-text"
  }, source.name), Noticia_jsx("a", {
    href: "{url}",
    target: "_blank",
    rel: "noopener noreferrer",
    className: "btn btn-info d-block"
  }, "Leer Noticia"))));
};

/* harmony default export */ var components_Noticia = (Noticia);
// CONCATENATED MODULE: ./components/Noticias.js

var Noticias_jsx = external_react_default.a.createElement;


const Noticias = props => {
  return Noticias_jsx("div", null, Noticias_jsx("div", {
    className: "row"
  }, props.noticias.map(noticia => Noticias_jsx(components_Noticia, {
    key: noticia.url,
    noticia: noticia
  }))));
};

/* harmony default export */ var components_Noticias = (Noticias);
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(keys);

// CONCATENATED MODULE: ./components/Evento.js

var Evento_jsx = external_react_default.a.createElement;

const Evento = props => {
  const {
    name,
    url,
    description
  } = props.info;
  let desc = description.text;
  let title = name.text.substr(0, 150) + '...';

  if (desc) {
    desc = desc.substr(0, 250) + '...';
  }

  return Evento_jsx("a", {
    href: "{url}",
    target: "_blank",
    rel: "noopener noreferrer",
    className: "list-group-item active text-light mb-1"
  }, Evento_jsx("h5", {
    className: "mb-3"
  }, name.text), Evento_jsx("p", {
    className: "mb-1"
  }, desc));
};

/* harmony default export */ var components_Evento = (Evento);
// CONCATENATED MODULE: ./components/Eventos.js


var Eventos_jsx = external_react_default.a.createElement;


const Eventos = props => {
  const eventosId = keys_default()(props.eventos);

  return Eventos_jsx("div", {
    className: "list-group"
  }, eventosId.map(key => Eventos_jsx(components_Evento, {
    key: key,
    info: props.eventos[key]
  })));
};

/* harmony default export */ var components_Eventos = (Eventos);
// EXTERNAL MODULE: external "isomorphic-unfetch"
var external_isomorphic_unfetch_ = __webpack_require__("0bYB");
var external_isomorphic_unfetch_default = /*#__PURE__*/__webpack_require__.n(external_isomorphic_unfetch_);

// CONCATENATED MODULE: ./pages/index.js

var pages_jsx = external_react_default.a.createElement;






const Index = props => pages_jsx(Master["a" /* default */], null, pages_jsx("div", {
  className: "row"
}, pages_jsx("div", {
  className: "col-12"
}, pages_jsx("h2", null, "Precio del Bitcoin"), pages_jsx(components_Precio, {
  precio: props.precioBitcoin
})), pages_jsx("div", {
  className: "col-md-8"
}, pages_jsx("h2", {
  className: "my-4"
}, "Noticias sobre bitcoin"), pages_jsx(components_Noticias, {
  noticias: props.noticiasBitcoin
})), pages_jsx("div", {
  className: "col-md-4"
}, pages_jsx("h2", {
  className: "my-4"
}, "Proximos eventos bitcoin"), pages_jsx(components_Eventos, {
  eventos: props.eventos
}))));

Index.getInitialProps = async () => {
  const precio = await external_isomorphic_unfetch_default()('https://api.coinmarketcap.com/v2/ticker/1/');
  const noticias = await external_isomorphic_unfetch_default()('https://newsapi.org/v2/everything?q=bitcoin&from=2019-09-14&sortBy=publishedAt&apiKey=5660a6bee8d8406ba9f82a6a5bc40ccf&language=es');
  const eventos = await external_isomorphic_unfetch_default()('https://www.eventbriteapi.com/v3/events/search/?token=YWNEBOWXZYCDNCFTH5RZ&locale=es_ES&q=bogota&sort_by=date&location.address=Colombia');
  const resPrecio = await precio.json();
  const resNoticias = await noticias.json();
  const resEventos = await eventos.json();
  return {
    precioBitcoin: resPrecio.data.quotes.USD,
    noticiasBitcoin: resNoticias.articles,
    eventos: resEventos.events
  };
};

/* harmony default export */ var pages = __webpack_exports__["default"] = (Index);

/***/ }),

/***/ "biE2":
/***/ (function(module, exports) {

module.exports = require("next/Link");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "pJ0N":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "next/Link"
var Link_ = __webpack_require__("biE2");
var Link_default = /*#__PURE__*/__webpack_require__.n(Link_);

// CONCATENATED MODULE: ./components/Navegacion.js

var __jsx = external_react_default.a.createElement;


const Navegacion = () => __jsx("nav", {
  className: "navbar navbar-expand navbar-dark bg-warning"
}, __jsx("div", {
  className: "container"
}, __jsx(Link_default.a, {
  href: "/"
}, __jsx("a", {
  className: "navbar-brand"
}, "TodoBitcoin")), __jsx("div", {
  className: "collapse navbar-collapse"
}, __jsx("ul", {
  className: "navbar-nav ml-auto"
}, __jsx("li", {
  className: "nav-item"
}, __jsx(Link_default.a, {
  href: "/"
}, __jsx("a", {
  className: "nav-link"
}, "Inicio"))), __jsx("li", {
  className: "nav-item"
}, __jsx(Link_default.a, {
  href: "/nosotros"
}, __jsx("a", {
  className: "nav-link"
}, "Nosotros")))))));

/* harmony default export */ var components_Navegacion = (Navegacion);
// CONCATENATED MODULE: ./components/Master.js

var Master_jsx = external_react_default.a.createElement;



const Master = props => Master_jsx("div", null, Master_jsx(head_default.a, null, Master_jsx("title", null, "BitCoin App"), Master_jsx("link", {
  rel: "stylesheet",
  href: "https://bootswatch.com/4/yeti/bootstrap.min.css"
})), Master_jsx(components_Navegacion, null), Master_jsx("div", {
  className: "container mt-4"
}, props.children));

/* harmony default export */ var components_Master = __webpack_exports__["a"] = (Master);

/***/ }),

/***/ "pLtp":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("qJj/");

/***/ }),

/***/ "qJj/":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ })

/******/ });