
const Evento = (props) => {
    const {name, url, description} = props.info;

    let desc = description.text;
    let title = name.text.substr(0,150) + '...';

    if(desc){
        desc = desc.substr(0,250) + '...';
    }

    return (
        <a href="{url}" target="_blank" rel="noopener noreferrer" className="list-group-item active text-light mb-1">
            <h5 className="mb-3">{name.text}</h5>
            <p className="mb-1">{desc}</p>
        </a>
    );
};

export default Evento;