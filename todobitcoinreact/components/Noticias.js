import Noticia from './Noticia';

const Noticias = (props) => {
    return (
        <div>
            <div className="row">
                {props.noticias.map((noticia) => (
                    <Noticia
                        key={noticia.url}
                        noticia = {noticia}
                    />
                ))}
            </div>
        </div>
    );
};

export default Noticias;