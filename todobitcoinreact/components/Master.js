import Head from 'next/head';
import Navegacion from './Navegacion';

const Master = (props) => (
    <div>
        <Head>
            <title>BitCoin App</title>
            <link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css"/>
        </Head>
        
        <Navegacion/>
        <div className="container mt-4">
            {props.children}
        </div>
    </div>
)

export default Master;