import Master from '../components/Master';
import Precio from '../components/Precio';
import Noticias from '../components/Noticias';
import Eventos from '../components/Eventos';

import fetch from 'isomorphic-unfetch';

const Index = (props) => (
    <Master>
        <div className="row">
            <div className="col-12">
                <h2>Precio del Bitcoin</h2>
                <Precio 
                    precio = {props.precioBitcoin}
                />
            </div>
            <div className="col-md-8">
                <h2 className="my-4">Noticias sobre bitcoin</h2>
                <Noticias 
                    noticias = {props.noticiasBitcoin}
                />
            </div>
            <div className="col-md-4">
                <h2 className="my-4">Proximos eventos bitcoin</h2>
                <Eventos 
                    eventos= {props.eventos}
                />
            </div>
        </div>
    </Master>
)

Index.getInitialProps = async () => {
    const precio = await fetch('https://api.coinmarketcap.com/v2/ticker/1/');
    const noticias = await fetch('https://newsapi.org/v2/everything?q=bitcoin&from=2019-09-14&sortBy=publishedAt&apiKey=5660a6bee8d8406ba9f82a6a5bc40ccf&language=es');
    const eventos = await fetch('https://www.eventbriteapi.com/v3/events/search/?token=YWNEBOWXZYCDNCFTH5RZ&locale=es_ES&q=bogota&sort_by=date&location.address=Colombia');

    const resPrecio = await precio.json();
    const resNoticias = await noticias.json();
    const resEventos = await eventos.json();

    return {
        precioBitcoin: resPrecio.data.quotes.USD,
        noticiasBitcoin: resNoticias.articles,
        eventos: resEventos.events
    }
}

export default Index;