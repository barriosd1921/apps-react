import Master from '../components/Master';

const Nosotros = () => (
    <Master>
        <div>
            <h2 className="text-center">Sobre Nosotros</h2>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur magnam eveniet eaque tempore blanditiis eum quos quae voluptas quo molestias neque quibusdam sint, ipsum aut aliquam totam assumenda consequatur harum.</p>

            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur magnam eveniet eaque tempore blanditiis eum quos quae voluptas quo molestias neque quibusdam sint, ipsum aut aliquam totam assumenda consequatur harum.</p>

            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur magnam eveniet eaque tempore blanditiis eum quos quae voluptas quo molestias neque quibusdam sint, ipsum aut aliquam totam assumenda consequatur harum.</p>
        </div>
    </Master>
)

export default Nosotros;