import React, {Fragment, useState} from 'react';

const Formulario = ({crearCita}) => {

    const stateClean = {
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
    };

    const [cita, setCita] = useState(stateClean);

    const handleChange = (e) => {
        setCita({
            ...cita,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        // Pasar la cita hacia el componente principal
        crearCita(cita);

        // Reiniciar el state
        setCita(stateClean);
    }

    return (
        <Fragment>
            <h2>Crear Cita</h2>

            <form
                onSubmit={handleSubmit}
            >
                <label>Nombre Mascota</label>
                <input 
                    type="text" 
                    name="mascota"
                    className="u-full-width" 
                    placeholder="Nombre Mascota"
                    value={cita.mascota}
                    onChange={handleChange}
                />

                <label>Nombre Dueño</label>
                <input 
                    type="text" 
                    name="propietario"
                    className="u-full-width"  
                    placeholder="Nombre Dueño de la Mascota"
                    value={cita.propietario}
                    onChange={handleChange}
                />

                <label>Fecha</label>
                <input 
                    type="date" 
                    className="u-full-width"
                    name="fecha"
                    value={cita.fecha}
                    onChange={handleChange}
                />               

                <label>Hora</label>
                <input 
                    type="time" 
                    className="u-full-width"
                    name="hora" 
                    value={cita.hora}
                    onChange={handleChange}
                />

                <label>Sintomas</label>
                <textarea 
                    className="u-full-width"
                    name="sintomas"
                    value={cita.sintomas}
                    onChange={handleChange}
                ></textarea>

                <button type="submit" className="button-primary u-full-width">Agregar</button>
            </form>
        </Fragment>
    );
};

export default Formulario;