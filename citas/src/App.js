import React, { useState, useEffect, Fragment } from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';

function App() {
  // Cargar citas el storage como stage inicial
  let citasIniciales = JSON.parse(localStorage.getItem('citas'));

  if(!citasIniciales) citasIniciales = [];

  // useState Creando con hooks
  const [citas, setCitas] = useState(citasIniciales);

  useEffect (
    () => {
      let citasInicales =  JSON.parse(localStorage.getItem('citas'));

      if(citasInicales){
        localStorage.setItem('citas',JSON.stringify(citas));
      } else {
        localStorage.setItem('citas',JSON.stringify([]));
      }
    }, [citas] )

  // Metodo para agregar nuevas citas al state
  const crearCita = cita => {
    // Tomar copia del state y agregar nuevo cliente
    const nuevasCitas = [...citas, cita];

    setCitas(nuevasCitas);
  }

  const eliminarCita = (index) => {
    const nuevasCitas = [...citas];
    nuevasCitas.splice(index,1);
    setCitas(nuevasCitas);
  }

  // cargar condicionalmente un titulo
  const titulo = Object.keys(citas).length === 0 ? 'No hay citas':'Administrar las citas';

  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>

      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario 
              crearCita={crearCita}
            />
          </div>
          <div className="one-half column">
            <h2>{titulo}</h2>
            {citas.map((cita, index) => (
              <Cita 
                key={index}
                index={index}
                cita={cita}
                eliminarCita={eliminarCita}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
