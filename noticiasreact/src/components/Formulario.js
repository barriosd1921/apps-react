import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Formulario extends Component {
    state = {
        categoria: 'general'
    }

    handleCategoria = e => {
        this.setState({
            categoria: e.target.value
        }, () => {
            // Parsarlo a la pagina principal
            // Hago llamado de funcion de callback, cada que se 
            // se termine de asignar el state, llama el metodo
            // consultar Noticias
            this.props.consultarNoticias(this.state.categoria);
        });        
    }

    render() {
        return (
            <div className="buscador row">
                <div className="col s12 m8 offset-m2">
                    <form>
                        <h2>Encuentra Noticias por Categoria</h2>
                        <div className="input-field col s12 m8 offset-m2">
                            <select
                                onChange={this.handleCategoria}
                            >
                                <option value="general">General</option>
                                <option value="business">Negocios</option>
                                <option value="entretaiment">Entretenimiento</option>
                                <option value="health">Salud</option>
                                <option value="science">Ciencia</option>
                                <option value="technology">Tecnologia</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

Formulario.propTypes = {
    consultarNoticias: PropTypes.func.isRequired
}

export default Formulario;