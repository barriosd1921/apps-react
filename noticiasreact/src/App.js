import React, { Component, Fragment } from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListaNoticias from './components/ListaNoticias';

class App extends Component {

  state = { 
    noticias: []
  }

  componentDidMount() {
    this.consultarNoticias();
  }
  
  consultarNoticias = async (categoria = 'general') => {
    const url = `https://newsapi.org/v2/top-headlines?country=ve&category=${categoria}&apiKey=5660a6bee8d8406ba9f82a6a5bc40ccf`;
  
    const respuesta = await fetch(url);
    const noticias = await respuesta.json();

    this.setState({
      noticias: noticias.articles
    });
  }

  render() {
    return (
      <Fragment>
        <Header 
          titulo="Noticias React API"
        />

        <div className="container white contenedor-noticias">
          <Formulario 
            consultarNoticias={this.consultarNoticias}
          />
          <ListaNoticias 
            noticias={this.state.noticias}
          />
        </div>
      </Fragment>
    );
  }
}

export default App;