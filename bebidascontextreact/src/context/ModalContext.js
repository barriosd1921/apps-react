import React, { createContext, useEffect, useState } from 'react';
import axios from 'axios';

export const ModalContext = createContext();

const ModalProvider = (props) => {

    const [idReceta, setIdReceta] = useState(null);
    const [recetaDetail, setRecetaDetail ] = useState({});

    useEffect(() => {
        const getDetailCocktail = async () => {
            if(!idReceta) return;

            const url=`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idReceta}`;

            const response = await axios.get(url);

            setRecetaDetail(response.data.drinks[0]);
        }

        getDetailCocktail();

    }, [idReceta]);
    return (
        <ModalContext.Provider
            value={{
                recetaDetail,
                setIdReceta                
            }}
        >
            {props.children}
        </ModalContext.Provider>
    );
};

export default ModalProvider;