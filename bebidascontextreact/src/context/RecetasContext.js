import React, { createContext, useState, useEffect } from 'react';

import axios from 'axios';

export const RecetasContext = createContext();

const RecetasProvider = (props) => {

    const [ recetas,setRecetas ] = useState([]);
    const [ searchReceta,setSearchReceta ] = useState({
        nombre: '',
        categoria: ''
    });
    const [isLoad, setIsLoad ] = useState(false);

    useEffect(() => {

        if(isLoad){
            const getRecetas = async () => {
                const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${searchReceta.nombre}&c=${searchReceta.categoria}`;
                
                const response = await axios.get(url);
                getRecetas(false);
                setRecetas(response.data.drinks);
            };
    
            getRecetas();
        }        

    }, [searchReceta]);

    return (
        <RecetasContext.Provider
            value={{
                recetas,
                setSearchReceta,
                setIsLoad
            }}
        >
            {props.children}
        </RecetasContext.Provider>
    );
};

export default RecetasProvider;