import React, { createContext, useState, useEffect } from 'react';

import axios from 'axios';

// Crear el context
export const CategoriasContext = createContext();

// Crear Provider, es donde se encuentran las funciones y el state
const CategoriasProvider = (props) => {

    // crear state del context 
    const [categorias, setCategorias] = useState([]);

    // Ejecutar el llamado al api de cocteles 
    useEffect(() => {

        const getCategorias = async () => {
            const url = `https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list`;
            const categorias = await axios.get(url);

            setCategorias(categorias.data.drinks);
        };

        getCategorias();

    }, []);

    return (
        <CategoriasContext.Provider
            value={{
                categorias
            }}
        >
            {props.children}
        </CategoriasContext.Provider>
    );
}

export default CategoriasProvider;