import React, { useContext, useState } from 'react';

import { CategoriasContext } from '../context/CategoriasContext';
import { RecetasContext } from '../context/RecetasContext';

const Formulario = () => {

    const [search, setSearch ] = useState({
        nombre: '',
        categoria: ''
    });

    const { categorias } = useContext(CategoriasContext);
    const { setSearchReceta, setIsLoad } = useContext(RecetasContext);

    const getDatosReceta = e => {
        setSearch({
            ...search,
            [e.target.name] : e.target.value    
        });
    }

    return (
        <form 
            className="col-12"
            onSubmit={e => {
                e.preventDefault();
                setSearchReceta(search);
                setIsLoad(true);
            }}
        >
            <fieldset className="text-center">
                <legend>Busca bebidas por categoria o ingrediente</legend>
            </fieldset>

            <div className="row">
                <div className="col-md-4">
                    <input 
                        name="nombre" 
                        type="text" 
                        className="form-control" 
                        placeholder="Buscar por Ingrediente"
                        onChange={getDatosReceta}
                    />
                </div>
                <div className="col-md-4">
                    <select 
                        name="categoria"
                        className="form-control"
                        onChange={getDatosReceta}
                    >
                        <option value="">-- Seleccionar Categoria --</option>
                        {
                            categorias.map(categoria => (
                                <option 
                                    key={categoria.strCategory}
                                    value={categoria.strCategory}
                                >{categoria.strCategory}</option>
                            )) 
                        }
                    </select>
                </div>
                <div className="col-md-4">
                    <input 
                        type="submit"
                        className="btn btn-block btn-primary"
                        value="Buscar Bebidas"
                    />
                </div>
            </div>
        </form>
    );
};

export default Formulario;