import React, { useContext, useState } from 'react';

import { ModalContext } from '../context/ModalContext';

import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';

function getModalStyle() {
    const top = 50 ;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: 'absolute',
        width: 300,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        overflow: 'scroll',
        height: '100%',
        maxHeight: 500,
        display: 'block'
    },
    header: {
        padding: '12px 0',
        borderBottom: '1px solid darkgrey'
    },
    content: {
        padding: "12px 0",
        overflow: 'scroll'
    }
}));

const Receta = ({receta}) => {

    // Configurando modal de material-ui 
    const [ modalStyle ] = useState(getModalStyle);
    const [ open, setOpen ] = useState(false);

    const classes = useStyles();
    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const { recetaDetail, setIdReceta } = useContext(ModalContext);

    const mostrarIngredientes = info => {
        let ingredientes = [];
        for(let i = 1; i < 16; i++){
            if( info[`strIngredient${i}`] ) {
                ingredientes.push(
                    <li>{ info[`strIngredient${i}`] } - { info[`strMeasure${i}`] }</li>
                );
            }
        }

        return ingredientes;
    }

    return (
        <div className="col-md-4 mt-3">
            <div className="card">
                <h2 className="card-header">{receta.strDrink}</h2>
                <img 
                    src={receta.strDrinkThumb} 
                    alt={receta.strDrink} 
                    className="card-img-top"
                />
                <div className="card-body">
                    <button
                        type="button"
                        className="btn btn-primary btn-block"
                        onClick={e => {
                            e.preventDefault();
                            setIdReceta(receta.idDrink);
                            handleOpen();
                        }}
                    >Ver Receta</button>

                    <Modal
                        open={open}
                        onClose={ () => {
                            setIdReceta(null);
                            handleClose();
                        }}
                    >
                        <div style={modalStyle} className={classes.paper}>
                            <h2>{recetaDetail.strDrink}</h2>
                            <h3 className="mt-4">Instrucciones</h3>
                            <p>
                                {recetaDetail.strInstructions}
                            </p>
                            <img 
                                src={recetaDetail.strDrinkThumb} 
                                alt={recetaDetail.strDrink} 
                                className="img-fluid my-4"
                            />
                            <h3>Ingredientes y Cantidades</h3>
                            <ul>
                                { mostrarIngredientes(recetaDetail) }
                            </ul>
                        </div>
                    </Modal>
                </div>
            </div>
        </div>
    );
};

export default Receta;