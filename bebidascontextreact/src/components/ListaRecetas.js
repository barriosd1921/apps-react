import React, { useContext } from 'react';

import Receta from './Receta';

import { RecetasContext } from '../context/RecetasContext';

const ListaRecetas = () => {

    const { recetas } = useContext(RecetasContext);

    return (
        <div className="row mt-5">
            {recetas.map(item => (
                <Receta 
                    key={item.idDrink}
                    receta={item}
                />
            ))}
        </div>
    );
};

export default ListaRecetas;