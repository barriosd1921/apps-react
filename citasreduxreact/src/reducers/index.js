import {combineReducers} from 'redux';
import citasRecuder from './citasReducer';
import validationReducer from './validationReducer';

export default combineReducers({
    citas: citasRecuder,
    error: validationReducer
});